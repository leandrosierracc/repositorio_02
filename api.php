<?php

// REST (Representational State Transfer) allows anything to work with your data // that can send a HTTP request

// The most common methods used are GET, POST, PUT, and DELETE
// GET: Used to retrieve data from a resource
// POST: Used to create a new resource, but is considered unsafe
// PUT: Used to update a resource, but is considered unsafe
// DELETE: Used to delete a resource and also is unsafe

// ********************************
function alta_nuevo_actor($nombre, $anno_oscar, $pelicula) {
	// Aquí se hará las SQL para insertarlo en la BD
	return "Insertando en la BD:".$nombre.",".$anno_oscar.",".$pelicula;
}

// ********************************
function get_datos_actor($id){
    $arrActores = array();
     // Los datos normalmente se sacaran de la BD
    switch($id){
        case 1:
            $arrActores = array("nombre" => "Humphrey Bogart", "anno_oscar" => "1951", "pelicula" => "La reina de Africa"); 
            break;
        case 2:
            $arrActores = array("nombre" => "Gary Cooper", "anno_oscar" => "1952", "pelicula" => "Solo ante el peligro"); 
            break;
		case 3:
            $arrActores = array("nombre" => "William Holden", "anno_oscar" => "1953", "pelicula" => "Traidor en el infierno"); 
            break;
   }
   return $arrActores;
}

// ********************************
function get_listado_actores() {
    // Los datos normalmente se sacaran de la BD
    $arrActores = array(array("id" => 1, "nombre" => "Humphrey Bogart"),
                          array("id" => 2, "nombre" => "Gary Cooper"),
                          array("id" => 3, "nombre" => "William Holden"));
    
    return $arrActores;
}

// *** PRINCIPAL
if(isset($_GET["accion"])){ // Siempre debe existir
    switch($_GET["accion"]){
		case "alta_nuevo_actor":
            $value = alta_nuevo_actor($_GET["nombre"], $_GET["anno_oscar"], $_GET["pelicula"]);
            break;
        case "get_listado_actores":
            $value = get_listado_actores();
            break;
         case "get_datos_actor":
            $value = get_datos_actor($_GET["id"]);
            break;
    }
}
exit(json_encode($value)); // tiene que ser exit, no echo
?>